package new_shop.controllers;

import new_shop.model.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

@Controller
@RequestMapping("catalog/products")
public class ProductController {

    ProductRepository productRepository;

    @Autowired
    public ProductController(ProductRepository productRepository) {

        this.productRepository = productRepository;
    }

    @GetMapping("list")
    ModelAndView list(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("catalog/product/list");
        modelAndView.addAllObjects(Map.of("products", this.productRepository.findAll()));
        return modelAndView;
    }



}
